import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private lat: number;
  private lng: number;
  private apiKey = environment.apiWeatherKey;
  private apiUrl = environment.apiWeatherUrl;

  constructor(private http: HttpClient) { }

  getWeatherByLocation(lat: number, lng: number): Observable<any> {

    this.lat = lat;
    this.lng = lng;

    const url = this.apiUrl + `?lat=${this.lat}&lon=${this.lng}&appid=${this.apiKey}&units=metric`;

    return this.http.get(url);
  }
}
