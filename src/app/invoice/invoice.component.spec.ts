import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InvoiceComponent } from './invoice.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { CurrencyFormatPipe } from '../shared/pipes/currency-format.pipe';
import { InvoiceService } from './invoice.service';
import { WeatherComponent } from '../weather/weather.component';
import { HttpClientModule } from '@angular/common/http';

describe('InvoiceComponent', () => {
  let component: InvoiceComponent;
  let fixture: ComponentFixture<InvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyFormatPipe, InvoiceComponent, WeatherComponent, InvoiceListComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule
      ],
      providers: [InvoiceService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
