import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceDetailComponent } from './invoice-detail.component';
import { InvoiceService } from '../invoice.service';
import { HttpClientModule } from '@angular/common/http';
import { CurrencyFormatPipe } from '../../shared/pipes/currency-format.pipe';
import { InvoiceListComponent } from '../invoice-list/invoice-list.component';

describe('InvoiceDetailComponent', () => {
  let component: InvoiceDetailComponent;
  let fixture: ComponentFixture<InvoiceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyFormatPipe, InvoiceListComponent, InvoiceDetailComponent],
      providers: [InvoiceService],
      imports: [HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
