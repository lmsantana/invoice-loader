export class InvoiceModel {
  idInvoice: number;
  numberInvoice: number;
  net: number;
  tax: number;
  netTax: number;
  total: number;
}
