import { Component, OnInit } from '@angular/core';
import { WeatherService } from './weather.service';

const dayRange = 3;

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  public imageBanner = '../assets/image_banner.png';
  private lat;
  private lng;
  listWeatherDetail: any = {
    country: '',
    name: '',
    coord: {
      lat: '',
      lon: ''
    }
  };
  listWeather: any;

  constructor(private httpWeatherService: WeatherService) { }

  ngOnInit() {
    this.getLocation();
  }

  protected getLocation() {

    if (navigator.geolocation) {

      navigator.geolocation.getCurrentPosition((position: Position) => {

        if (position) {
          console.log('Latitude: ' + position.coords.latitude +
            'Longitude: ' + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.getWeather();
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }

  protected getWeather() {
    this.httpWeatherService.getWeatherByLocation(this.lat, this.lng).subscribe(response => {
      this.listWeatherDetail = response.city;
      this.listWeather = this.filterDataLocation(response);
    });
  }

  protected filterDataLocation(response: any) {

    const dateAdded = new Date();
    dateAdded.setDate(dateAdded.getDate() + dayRange);
    const ts = Math.round(dateAdded.getTime() / 1000);
    const dateFiltered = [];

    response.list.filter(data => {
      if (data.dt <= ts) {
        return data;
      }
    }).forEach(element => {
      const dateElements = new Date(element.dt_txt).getDate();
      if (dateFiltered.length > 0) {
        const existInArray = dateFiltered.findIndex(i => new Date(i.dt_txt).getDate() === dateElements);
        if (existInArray < 0) {
          dateFiltered.push(element);
        }
      } else {
        dateFiltered.push(element);
      }
    });

    return dateFiltered;
  }
}
