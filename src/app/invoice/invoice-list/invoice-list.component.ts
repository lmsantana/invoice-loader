import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InvoiceService } from '../invoice.service';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.css']
})
export class InvoiceListComponent implements OnInit {

  invoiceLists: Array<any> = [];

  @Input() public showActions = true;
  @Input() public showTotal = false;
  @Input() public invoiceData;
  @Output() public invoiceCountList = new EventEmitter() ;

  constructor(private httpInvoice: InvoiceService) {
  }

  ngOnInit() {
    this.getInvoices();
  }

  getInvoices() {
    this.invoiceLists = this.httpInvoice.getInvoices();
    this.invoiceCountList.emit((this.invoiceLists).length);
  }

  deleteInvoice(id: number, title: any) {

    const confirm = window.confirm('Delete invoice number [' + title + '] ?');
    if (!!confirm) {
      this.httpInvoice.deleteInvoice(id);
    }
  }
}
