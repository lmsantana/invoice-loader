import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InvoiceService } from './invoice.service';
import { InvoiceModel } from './invoice.model';
import { CurrencyFormatPipe } from '../shared/pipes/currency-format.pipe';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  providers: [CurrencyFormatPipe]
})
export class InvoiceComponent implements OnInit {

  invoiceForm: FormGroup;
  taxList: Array<number> = [];
  net = 0;
  invoicesList: Array<any> = [];
  totalInvoices = 0;

  constructor(private httpInvoice: InvoiceService, public formBuilder: FormBuilder, private currency: CurrencyFormatPipe) {

  }

  ngOnInit() {
    this.getTaxes();
    this.initForm();
  }

  initForm() {
    this.invoiceForm = this.formBuilder.group({
      numberInvoice: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      net: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      taxList: [null, [Validators.required]],
      total: [null, [Validators.required]]
    });
  }

  invoiceCountEmited($event) {
    this.totalInvoices = $event;
  }

  restForm() {
    this.initForm();
  }

  getTaxes() {
    this.taxList = this.httpInvoice.getTaxes();
  }

  calculateTax() {
    const form = this.invoiceForm.value;
    const net = parseFloat(form.net);
    const taxSelected = parseFloat(form.taxList);
    const total = (taxSelected > 0) ? (net + ((net * taxSelected) / 100)) : net;
    this.invoiceForm.get('total').setValue(total);
    // const newNet = this.currency.transform(net);
    const newNet = (net);
    this.invoiceForm.get('net').setValue(newNet);
    this.net = net;
  }

  addInvoice() {
    const form = ((this.invoiceForm.value));
    form.net = this.net;
    const invoice = new InvoiceModel();
    const payload: InvoiceModel = Object.assign(invoice, form);
    payload.idInvoice = this.getRandomInteger();
    payload.tax = form.taxList;
    payload.netTax = payload.total - payload.net;
    this.httpInvoice.addInvoice(payload);
    this.invoiceForm.reset();
    this.totalInvoices++;
  }

  getRandomInteger() {
    const x = 6;
    const rand = Math.floor(Math.random() * x) + 1;
    return rand;
  }
}
