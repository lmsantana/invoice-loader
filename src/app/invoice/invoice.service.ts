import { Injectable } from '@angular/core';
import { InvoiceModel } from './invoice.model';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  private taxes: Array<number> = [0, 10.5, 20.0, 27.0];
  private invoicesList: InvoiceModel[] = [];

  constructor() { }

  public addInvoice(invoice: InvoiceModel) {

    this.invoicesList.push(invoice);
    this.refreshInvoices();
  }

  public getInvoices() {

    if (localStorage.getItem('invoice')) {
      const strInvoice = localStorage.getItem('invoice');
      const objInvoice = JSON.parse(strInvoice);
      this.invoicesList = Object.assign(this.invoicesList, objInvoice);
    }
    return this.invoicesList;
  }

  public deleteInvoices(): boolean {

    this.invoicesList = [];
    this.refreshInvoices();
    return true;
  }

  public getTaxes() {
    return this.taxes;
  }

  public deleteInvoice(index) {
    this.invoicesList.splice(index, 1);
    this.refreshInvoices();
  }

  refreshInvoices() {
    localStorage.setItem('invoice', JSON.stringify(this.invoicesList));
  }
}
