import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyFormat'
})
export class CurrencyFormatPipe implements PipeTransform {

  transform(value: any): any {

    const val = parseFloat(value);
    const p = val.toFixed(2).split('.');
    const format = '$ ' + p[0].split('').reverse().reduce((acc, num, i, orig) => {
      return num === '-' ? acc : num + (i && !(i % 3) ? '.' : '') + acc;
    }, '') + ',' + p[1];

    return format;
  }

}
