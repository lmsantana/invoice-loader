import { Component, OnInit } from '@angular/core';
import { InvoiceService } from '../invoice.service';

@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.css']
})
export class InvoiceDetailComponent implements OnInit {

  invoiceData = {
    invoiceCount: 0,
    netTotal: 0,
    taxtTotal: 0,
    invoiceTotal: 0
  };

  constructor(private httpInvoice: InvoiceService) {

  }

  ngOnInit() {
    this.getInvoices();
  }

  getInvoices() {

    const invoiceLists = this.httpInvoice.getInvoices();
    let net = 0;
    let netTax = 0;
    let total = 0;

    invoiceLists.forEach(data => {

      for (const objectName in data) {

        if (!(/^idInvoice|numberInvoice|taxList|tax/).test(objectName)) {

          switch (objectName) {
            case 'net':
                net += data[objectName];
                break;
            case 'netTax':
                netTax += data[objectName];
                break;
            default:
                total += data[objectName];
                break;
          }
        }
      }
    });

    this.invoiceData.invoiceCount = invoiceLists.length;
    this.invoiceData.netTotal = net;
    this.invoiceData.taxtTotal = netTax;
    this.invoiceData.invoiceTotal = total;
  }

  restData() {

    const responseDeleteInvoice = this.httpInvoice.deleteInvoices();
    if (responseDeleteInvoice) {
      window.location.href = 'invoices';
    }
  }
}
