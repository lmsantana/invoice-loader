import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { WeatherComponent } from './weather/weather.component';
import { InvoiceListComponent } from './invoice/invoice-list/invoice-list.component';
import { CurrencyFormatPipe } from './shared/pipes/currency-format.pipe';
import { InvoiceDetailComponent } from './invoice/invoice-detail/invoice-detail.component';
import { InvoiceService } from './invoice/invoice.service';
import { WeatherService } from './weather/weather.service';

@NgModule({
  declarations: [
    AppComponent,
    InvoiceComponent,
    WeatherComponent,
    InvoiceListComponent,
    CurrencyFormatPipe,
    InvoiceDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    InvoiceService,
    WeatherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
